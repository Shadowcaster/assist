@extends('layouts.app')

@section('content')
    <h1>{{$post->title}}</h1>
    
    <div class="qwe">
        {!!$post->body!!}
    </div>
    <hr>
    
    <small>Written on {{$post->created_at}} by {{$post->user->name}}</small>
    
    <hr>
    @if(!Auth::guest())
        @if (Auth::user()->id == $post->user_id || Auth::user()->hasRole('Admin'))
            <a href="/posts/{{$post->id}}/edit" class="btn btn-primary">Edit</a>
            
            {!!Form::open(['action'=> ['PostController@destroy',$post->id],'method'=> 'POST', 'class'=> 'float-right'])!!}
                {{Form::hidden('_method','DELETE')}}
                {{Form::submit('Delete',['class'=> 'btn btn-danger'])}}
            {!!Form::close()!!}
        @endif
    @endif
    
    <a role="button" href="/posts" class="btn btn-primary">Go Back</a>
    @include('inc.highlight')
@endsection