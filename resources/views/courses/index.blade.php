@extends('layouts.app')
@section('content')
    <h1>Courses</h1>
    <table id="table" class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Year</th>
                @if(!Auth::guest())
                    <th scope="col"></th>
                    @if (Auth::User()->hasRole('Admin'))
                        <th scope="col"></th>
                    @endif
                    @if (Auth::User()->hasRole('Asisten'))
                        <th scope="col"></th>
                    @endif
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach($courses as $course)
            @include('inc.modal_enroll')
                <tr>
                    <td><a href="/courses/{{$course->id}}">{{$course->name}}</a></td>
                    <td>{{$course->year}}</td>
                    
                    @if(!Auth::guest())
                            <td><button type="button" class="btn btn-primary" 
                                data-toggle="modal" 
                                data-target="#enroll"
                                data-title="{{$course->name}}"
                                data-year={{$course->year}}>Enroll</button></td>                        
                        @if (Auth::User()->hasRole('Admin'))
                            <td>
                                {!!Form::open(['action'=> ['CourseController@destroy',$course->id],'method'=> 'POST'])!!}
                                    {{Form::hidden('_method','DELETE')}}
                                    {{Form::submit('Delete',['class'=> 'btn btn-danger'])}}
                                {!!Form::close()!!}
                            </td>
                        @endif
                        @if (Auth::User()->hasRole('Asisten'))
                        <td><a class="btn btn-primary" href="{{action('CourseController@enrollAsisten',['course_id'=>$course->id])}}" role="button">Enroll as Asisten</a></td>
                        @endif
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>

    
    
    @include('inc.datatable_course_index')
@endsection