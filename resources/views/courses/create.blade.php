@extends('layouts.app')

@section('content')
    <h1>Create Course</h1>
    {!! Form::open(['action' => 'CourseController@store','method' => 'POST']) !!}
        <div class="form-group">
            {{Form::label('name','Name')}}
            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Name'])}}
        </div>
        <div class="form-group">
            {{Form::label('year','Year')}}
            {{Form::text('year', '', ['class' => 'form-control', 'placeholder' => 'Year'])}}
        </div>
        <div class="form-group">
            {{Form::label('pertemuan','Jumlah Pertemuan')}}
            {{Form::text('pertemuan', '', ['class' => 'form-control', 'placeholder' => 'Contoh : 8'])}}
        </div>
        <div class="form-group">
            {{Form::label('token','Token')}}
            {{Form::text('token', '', ['class' => 'form-control', 'placeholder' => 'Contoh : asdf18'])}}
        </div>
        {{Form::submit('Submit',['class'=>'btn btn-default'])}}
    {!! Form::close() !!}
@endsection