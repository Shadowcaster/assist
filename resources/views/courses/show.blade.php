@extends('layouts.app')

@section('content')
    <h1>{{$course->name}}</h1>

    @php
        $isAsisten = false;
        foreach($asistens as $asisten) {
            if($asisten->getId() == auth()->user()->id) {
                $isAsisten = true;
                break;
            }
        }
    @endphp
    



    @if($isAsisten)
        <table id="table"
               class="table table-striped">
            <thead class="thead-dark">
                <th>Nim</th>
                <th>Name</th>
                <th>Asisten</th>
                <th>Pertemuan</th>
                <th>TP</th>
                <th>Respon</th>
                <th>Praktikum</th>

                <th></th>           
            </thead>
            <div id="but" class = "my-2"></div>
            <tbody>
                
                @foreach($users as $user)
                    @if ($user->getAsisten($course->id) !=null)
                        @php
                        $asName= $user->getAsisten($course->id)->getName();
                            
                        @endphp
                    @else
                       @php
                        $asName="None"   
                       @endphp
                    @endif
                    @foreach ($pertemuan as $item)
                        
                        @if ($item=="All")
                            @continue
                        @endif
                        <tr position="{{$item}}" assist="{{$asName}}">
                            <td>{{$user->nim}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$asName}}</td>
                            <td>{{$item}}</td>
                            <td>{{$user->getNilai($course->id)[$item-1]->tp}}</td>
                            <td>{{$user->getNilai($course->id)[$item-1]->respon}}</td>
                            <td>{{$user->getNilai($course->id)[$item-1]->praktikum}}</td>
                            <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit" 
                                data-tp="{{$user->getNilai($course->id)[$item-1]->tp}}" 
                                data-respon="{{$user->getNilai($course->id)[$item-1]->respon}}"
                                data-praktikum="{{$user->getNilai($course->id)[$item-1]->praktikum}}"
                                data-user_id="{{$user->id}}"
                                data-course_id="{{$course->id}}"
                                data-pertemuan="{{$item}}">Edit</button></td>
                        </tr>
                    @endforeach
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>Nim</th>
                    <th>Name</th>
                    <th>Asisten</th>
                    <th>Pertemuan</th>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tfoot>
        </table>

    <!-- If Not an Asisten -->
    @else

        <!-- If you have chosen an Asisten -->
        @if($haveChosenAsisten) 
            <p> Your Asisten : {{$asistenName}}</p>
            <h1>Your Nilai :</h1>
            <table class="table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th>Pertemuan</th>
                        <th>TP</th>
                        <th>Respon</th>
                        <th>Praktikum</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($pertemuan as $item)                        
                        @if ($item=="All")
                            @continue
                        @endif
                        <tr>                            
                            <td>{{$item}}</td>
                            <td>{{auth()->user()->getNilai($course->id)[$item-1]->tp}}</td>
                            <td>{{auth()->user()->getNilai($course->id)[$item-1]->respon}}</td>
                            <td>{{auth()->user()->getNilai($course->id)[$item-1]->praktikum}}</td>                            
                        </tr>
                    @endforeach
                    <tr></tr>
                </tbody>
            </table>

        <!-- If you have not chosen an Asisten -->
        @else
            <h2>Enrolled Asisten</h2>
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th>Nim</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($asistens as $asisten)             
                        <tr>
                            <td>{{$asisten->getNim()}}</td>
                            <td>{{$asisten->getName()}}</td>
                            <td><a class="btn btn-primary" 
                                href="{{action('CourseController@reg',['course_id'=>$course->id,'asisten_id'=>$asisten->id])}}" 
                                role="button">Register as Praktikan</a></td>
                        </tr>
                    @endforeach
                </tbody>
                
            </table>
        @endif
        
    @endif   
    
    <hr>
    <a role="button" href="/courses" class="btn btn-primary">Go Back</a>

    @include('inc.modal')
    
    @include('inc.datatable_course_show')
    

@endsection

