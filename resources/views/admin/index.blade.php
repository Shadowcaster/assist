@extends('layouts.app')
@section('content')
    <h1>Registered Users</h1>
    <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col">ID</th>
            <th scope="col">Nim</th>
            <th scope="col">Name</th>
            <th scope="col">Asisten</th>
            <th scope="col">Admin</th>
            <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)          
                <tr>
                    {!! Form::open(['action' => ['RolesController@assignRoles', $user->id],'method' => 'POST']) !!}
                    <td>{{$user->id}}</td>
                    <td>{{$user->nim}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{Form::checkbox('role_asisten', 'asisten',App\User::find($user->id)->hasRole('Asisten') ? true : false)}}</td>
                    <td>{{Form::checkbox('role_admin', 'admin',App\User::find($user->id)->hasRole('Admin') ? true : false)}}</td>
                    <td>{{Form::submit('Assign Roles',['class'=>'btn btn-primary'])}}</td>
                    {!! Form::close() !!}
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection