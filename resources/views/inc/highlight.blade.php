<link rel="stylesheet" href="{{ asset('vendor/highlight/styles/rainbow.css') }}">
<script src="{{ asset('vendor/highlight/highlight.pack.js') }}"></script>
<script>hljs.initHighlightingOnLoad();</script>