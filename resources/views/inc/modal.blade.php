<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit nilai</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['action' => ['CourseController@update',$course->id],'method' => 'POST']) !!}
            <div class="modal-body">
                <div class="form-group">
                    {{Form::label('tp','TP')}}
                    {{Form::text('tp', '', ['class' => 'form-control', 'placeholder' => 'Nilai TP'])}}
                </div>
                <div class="form-group">
                    {{Form::label('respon','Respon')}}
                    {{Form::text('respon', '', ['class' => 'form-control', 'placeholder' => 'Nilai Respon'])}}
                </div>
                <div class="form-group">
                    {{Form::label('praktikum','Praktikum')}}
                    {{Form::text('praktikum', '', ['class' => 'form-control', 'placeholder' => 'Nilai Praktikum'])}}
                </div>
                <div class="form-group">
                    {{Form::hidden('user_id','0', array('id' => 'user_id'))}}
                </div>
                <div class="form-group">
                    {{Form::hidden('course_id','0', array('id' => 'course_id'))}}   
                </div>
                <div class="form-group">
                    {{Form::hidden('pertemuan','0', array('id' => 'pertemuan'))}}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{Form::hidden('_method','PUT')}}
                {{Form::submit('Save Changes',['class'=>'btn btn-primary'])}}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<script>
    $('#edit').on('show.bs.modal', function (event) {
                console.log('asd');
                //get the button that triggered the modal
                var button = $(event.relatedTarget)
                
                //get data
                var tp = button.data('tp')
                var respon = button.data('respon')
                var praktikum = button.data('praktikum')
                var user_id = button.data('user_id');
                var course_id = button.data ('course_id');
                var pertemuan = button.data('pertemuan');
                //instance of the modal
                var modal = $(this)
                
                //title
                modal.find('.modal-title').text('Edit data')

                //append data
                modal.find('.modal-body #tp').val(tp)
                modal.find('.modal-body #respon').val(respon)
                modal.find('.modal-body #praktikum').val(praktikum)
                modal.find('.modal-body #user_id').val(user_id)
                modal.find('.modal-body #course_id').val(course_id)
                modal.find('.modal-body #pertemuan').val(pertemuan)
            })
</script>