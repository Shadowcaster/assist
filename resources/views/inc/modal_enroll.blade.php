<div class="modal fade" id="enroll" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Insert Token</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['action' => ['CourseController@enroll',$course->id],'method' => 'POST']) !!}
            <div class="modal-body">
                <div class="form-group">
                    {{Form::label('token','Token')}}
                    {{Form::text('token', '', ['class' => 'form-control', 'placeholder' => 'Token'])}}
                </div>  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{Form::submit('Enroll',['class'=>'btn btn-primary'])}}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<script>
    $('#enroll').on('show.bs.modal', function (event) {
            
            //get the button that triggered the modal
            var button = $(event.relatedTarget)
            
            var title = button.data('title')
            var year = button.data('year')

            title += ' (';
            title += year;
            title += ')'
            
            //instance of the modal
            var modal = $(this)
            
            //title
            modal.find('.modal-title').text(title)

        })
</script>