@include('inc.datatable')
<script>
    $(document).ready(function() {
        var table = $('#table').DataTable();
        
        new $.fn.dataTable.Buttons( table, {
            buttons: [
                'excel'
            ]
        } );
        
        table.buttons().container()
            .appendTo( $('#but') );

        $('#table tfoot th').each( function () {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        } );
        
        table.columns().every( function () {
            var that = this;
    
            $( 'input', this.footer() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                }
            } );
        } );
        
    } );
</script>