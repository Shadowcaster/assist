@extends('layouts.app')
@section('content')
    <h1>Asisten</h1>
    <p>Under Construction</p>
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Nim</th>
                <th scope="col">Name</th>
            </tr>
        </thead>
        <tbody>
            @foreach($asisten as $as)          
                <tr>
                    <td>{{$as->nim}}</td>
                    <td>{{$as->name}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection