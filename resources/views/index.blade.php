@extends('layouts.app')
@section('content')
    <h1 class="jumbotron text-center">Computer Science</h1>
    <img class="img-fluid" src="{{asset('storage/images/csmap.png')}}" alt="asd">
    <footer class="blockquote-footer">image taken from <a href="https://www.flickr.com/photos/95869671@N08/36231833334">Flickr</a> </footer>
@endsection
<footer class="container-fluid bg-dark fixed-bottom">
    <div class= "text-info text-center py-3" >
        <span data-toggle="tooltip" title="Gibran Zidane">Copyright &copy Meowulf</span>
    </div>
</footer>