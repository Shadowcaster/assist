@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif                 
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection

<footer class="container-fluid bg-dark fixed-bottom">
    <div class= "text-info text-center py-3" >
        <span data-toggle="tooltip" title="Gibran Zidane">Copyright &copy Meowulf</span>
    </div>
</footer>