<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user = Role::where('name','User')->first();
        $role_asisten = Role::where('name','Asisten')->first();
        $role_admin = Role::where('name','Admin')->first();

        $user = User::where('nim','H00000000')->first();
        $user->roles()->attach($role_admin);

        $user = User::where('nim','H13116304')->first();
        $user->roles()->attach($role_asisten);
    }
}
