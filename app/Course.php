<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Course extends Model
{
    public function users()
    {
        return $this->belongsToMany('App\User', 'enrollment')->as('enrollment');
    }
    public function asistens() {
        return $this->belongsToMany('App\Asisten', 'asisten_enrollment')->as('enrollment');
    }
}
