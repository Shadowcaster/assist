<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\User;
class Asisten extends Model
{
   

    public function courses()
    {
        return $this->belongsToMany('App\Course','asisten_enrollment')->as('enrollment');
    }

    public function getPraktikan($course_id) {
        $praktikan = collect(\DB::select('select user_id from praktikan where asisten_id = ? and course_id = ?', [$this->id, $course_id]));
        return $praktikan;
    }

    private function getInfo() {
        return User::find($this->user_id);
    }
    public function getName() {
        return $this->getInfo()->name;
    }
    public function getNim() {
        return $this->getInfo()->nim;
    }
    public function getId() {
        return $this->getInfo()->id;
    }
}
