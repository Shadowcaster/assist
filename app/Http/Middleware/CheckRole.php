<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if(Auth::check()) {
            if($request->user()->hasRole('Admin')) {
                return $next($request);
            }
            if(! $request->user()->hasRole($role) ) {
                return redirect('/dashboard')->with('error', 'Unauthorized Page');
            }
    
            return $next($request);
        }
        return redirect('/dashboard')->with('error', 'Unauthorized Page');
    }
}
