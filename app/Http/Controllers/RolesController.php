<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Asisten;
class RolesController extends Controller
{
    public function assignRoles(Request $request, $id) {
        $user = User::find($id);
        $user->roles()->detach();
        if($request['role_asisten']) {
            $user->roles()->attach(Role::where('name','Asisten')->first());

            $asisten = new Asisten();
            $asisten->user_id = $id;
            $asisten->save();
        } else {
            $asisten = Asisten::where('user_id',$id)->first();
            if($asisten!=null) {
                $asisten->delete();
            }
        }
        if($request['role_admin']) {
            $user->roles()->attach(Role::where('name','Admin')->first());
        }
        return redirect()->back()->with('success','Roles Updated');
    }
}
