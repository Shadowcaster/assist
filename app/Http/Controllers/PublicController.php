<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class PublicController extends Controller
{
    public function asisten() {
        $asisten = DB::select('select * from users, role_user where users.id = role_user.user_id and role_user.role_id = 2', [1]);
        return view('public.asisten')->with('asisten',$asisten);
    }
}
