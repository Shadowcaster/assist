<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Course;
use App\User;
use App\Asisten;

use App\Http\Middleware\Admin;
class CourseController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth',['except' => ['index']]);
        $this->middleware('role:Asisten',['only' =>['enrollAsisten','update']]);
        $this->middleware('role:Admin', ['except' => ['index', 'show', 'enroll', 'enrollAsisten','reg', 'update']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::orderBy('updated_at','desc')->paginate(10);
        return view('courses.index')->with('courses',$courses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('courses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'year' => 'required',
            'pertemuan' => 'required',
            'token' => 'required',
        ]);
        

        // Create Course
        $course = new Course();
        $course->name = $request->input('name');
        $course->year = $request->input('year');
        $course->pertemuan = $request->input('pertemuan');
        $course->token = $request->input('token');
        $course->save();

        return redirect('/courses')->with('success','Course Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course = Course::find($id);

        // jumlah pertemuan
        $pertemuan = array();
        $pertemuan['All'] = "All";
        for($i = 1; $i <=$course->pertemuan; $i++) {
            $pertemuan[$i] = $i;
        }

        // enrolled praktikan
        $users = $course->users;
        //enrolled asisten
        $asistens = $course->asistens;

        $asistenNames = array();
        $asistenNames['All'] = "All";
        foreach($asistens as $asisten) {
            $asistenNames[$asisten->getName()] = $asisten->getName();
        }
        $result = DB::table('praktikan')
            ->select('asisten_id')
            ->where('course_id',$id)
            ->where('user_id',auth()->user()->id)
            ->first();

        

        if($result==null) {
            $haveChosenAsisten = false;
            $asistenName = "";
        } else {
            $haveChosenAsisten = true;
            $asisten = Asisten::find($result->asisten_id);
            $asistenName = $asisten->getName();
        }
        
        /*
        foreach($users as $u) {
            echo $u->getAsisten($id)->asisten_id;
        }
        */
        //return User::find(5)->getAsisten($id)->asisten_id;
        //return Asisten::find(1)->getPraktikan($id);
        
        $data = array(
            'course' => $course,
            'pertemuan' => $pertemuan,
            'users' => $users,
            'asistens' => $asistens,
            'haveChosenAsisten' => $haveChosenAsisten,
            'asistenName' => $asistenName,
        );
        
        return view('courses.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tp = $request->input('tp');
        if($tp==null)
            $tp = 0;
        $respon = $request->input('respon');
        if($respon==null)
            $respon = 0;
        $praktikum = $request->input('praktikum');
        if($praktikum==null)
            $praktikum = 0;
        DB::update('update nilai set tp = ?, 
        respon = ?, 
        praktikum = ? 
        where user_id = ? and 
        course_id = ? and 
        pertemuan = ?', 
        [$tp, 
        $respon, 
        $praktikum, 
        $request->input('user_id'), 
        $id, 
        $request->input('pertemuan')]);
        return redirect()->back()->with('success','Nilai Updated');
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Course::find($id);
        
         // Check for correct user
        if(!auth()->user()->hasRole('Admin') ) {
            return redirect('/course')->with('error', 'Unauthorized Page');
        }

        $course->delete();
        return redirect('/courses')->with('success','Course Deleted');
    }

    public function enroll(Request $request, $course_id) {
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        
        if($user!=null) {

           
            foreach ($user->courses as $c) {
                if($c->enrollment->course_id == $course_id) {
                    return redirect()->back()->with('error','You have already enrolled to the course');
                }
            }
            
            $course = Course::find($course_id);
            if($course->token != $request->token) {
                return redirect()->back()->with('error','Wrong Token');
            }

            foreach($course->asistens as $asisten) {
                if($asisten->user_id == $user_id) {
                    return redirect()->back()->with('error','You have already enrolled to the course as Asisten');
                }
            }
            $user->courses()->attach($course);
            for($i = 1; $i <=$course->pertemuan; $i++) {
                DB::insert('insert into nilai (user_id, course_id, pertemuan) values (?, ?, ?)', [$user_id, $course_id,$i]);
            }      
            return redirect()->back()->with('success','Enrolled');  
        } else {
            return redirect()->back()->with('error','Unauthorized Page');  
        }
        
    }

    public function enrollAsisten($course_id) {
        $user_id = auth()->user()->id;
        $asisten = Asisten::where('user_id',$user_id)->first();
        if($asisten != null) {
            foreach ($asisten->courses as $course) {
                if($course->enrollment->course_id == $course_id) {
                    return redirect()->back()->with('error','You have already enrolled to the course');
                }
            }
            $asisten->courses()->attach(Course::find($course_id));
    
            return redirect()->back()->with('success','Enrolled as Asisten');
        }
        return redirect()->back()->with('error','You are not an Asisten, nice try');
    }
    public function reg($course_id,$asisten_id) {
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        $enrolled = false;
        foreach ($user->courses as $course) {
            if($course->enrollment->course_id == $course_id) {
                $enrolled = true;
                break;
            }
        }
        if($enrolled) {
            $result = DB::select('select * from praktikan where course_id = ? and user_id = ?', [$course_id, $user_id]);
            if($result == null) {
                DB::insert('insert into praktikan (course_id, user_id, asisten_id) values (?, ?, ?)', [$course_id, $user_id, $asisten_id]);
                return redirect()->back()->with('success','You have chosen your asisten');  
            } else {
                return redirect()->back()->with('error','You have already chosen your asisten');
            }
        } else {
            return redirect()->back()->with('error','You have not enrolled to the course');
        }
        
    }


}
