<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use App\Post;
use App\Role;
use App\Course;
use App\Asisten;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'nim', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    

    public function posts() {
        return $this->hasMany('App\Post');
    }

    public function roles() {
        return $this->belongsToMany('App\Role');
    }

    public function courses() {
        return $this->belongsToMany('App\Course','enrollment')->as('enrollment');
    }

    public function hasRole($role) {
        if($this->roles()->where('name',$role)->first()) {
            return true;
        }
        return false;
    }

    public function isAdmin() {
        return $this->hasRole('Admin');
    }
    
    public function isAsisten() {
        return $this->hasRole('Asisten');
    }

    public function getAsisten($course_id) {
        $asisten = collect(\DB::select('select asisten_id from praktikan where user_id = ? and course_id = ?', [$this->id, $course_id]))->first();
        if($asisten != null)
            return Asisten::find($asisten->asisten_id);
        else
            return null;
    }

    public function getNilai($course_id) {
        $nilai = collect(\DB::select('select pertemuan, tp, respon, praktikum from nilai where user_id = ? and course_id = ?', [$this->id, $course_id]));
        return $nilai;
    }
}
