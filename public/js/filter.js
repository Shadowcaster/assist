$(document).ready(function() {

    var rows = $("table#table tbody tr");

    $("#pertemuan").on("change", function() {

        var asistenSelected = $("#asisten")[0].value;
        var selected = this.value;
        console.log(asistenSelected)
        console.log(selected)
        if (selected != "All" && asistenSelected != "All") {
            
            rows.filter("[position=" + selected + "][assist='" + asistenSelected + "']").show();
            rows.not("[position=" + selected + "][assist='" + asistenSelected + "']").hide();
            
        } else if (selected == "All" && asistenSelected !="All") {
            rows.filter("[assist='" + asistenSelected + "']").show();
            rows.not("[assist='" + asistenSelected + "']").hide();
        } else if(selected != "All" && asistenSelected =="All") {
            rows.filter("[position=" + selected + "]").show();
            rows.not("[position=" + selected + "]").hide();
        } else {
            rows.show();
        }

    });

    $("#asisten").on("change", function() {

        var asistenSelected = this.value;
        var selected = $("#pertemuan")[0].value;
        console.log(asistenSelected)
        console.log(selected)

        if (selected != "All" && asistenSelected != "All") {

            rows.filter("[assist='" + asistenSelected + "'][position=" + selected + "]").show();
            rows.not("[assist='" + asistenSelected + "'][position=" + selected + "]").hide();
            
        } else if (selected == "All" && asistenSelected !="All") {
            rows.filter("[assist='" + asistenSelected + "']").show();
            rows.not("[assist='" + asistenSelected + "']").hide();
        } else if(selected != "All" && asistenSelected =="All") {
            rows.filter("[position=" + selected + "]").show();
            rows.not("[position=" + selected + "]").hide();
        } else {
            rows.show();
        }

    });


    $("#search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("table#table tbody tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});