<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/asisten', 'PublicController@asisten');

Route::resource('posts','PostController');
Route::resource('courses','CourseController');
Route::resource('distributions','DistributionController');

Auth::routes();
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

Route::get('/admin','DashboardController@admin')->middleware('role:Admin');
Route::get('/praktikan','DashboardController@praktikan')->middleware('role:Asisten');

Route::get('/admin/{id}', 'DashboardController@admin')->middleware('role:Admin');
Route::post('/admin/{id}', 'RolesController@assignRoles')->middleware('role:Admin');

Route::get('/courses/{course_id}/enroll', 'CourseController@enroll');
Route::post('/courses/{course_id}/enroll', 'CourseController@enroll');

Route::get('/courses/asisten/{course_id}', 'CourseController@enrollAsisten')->middleware('role:Asisten');

Route::get('/courses/{course_id}/reg/{asisten_id}', 'CourseController@reg');
